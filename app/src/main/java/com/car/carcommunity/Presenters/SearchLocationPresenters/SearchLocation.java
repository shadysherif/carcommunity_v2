package com.car.carcommunity.Presenters.SearchLocationPresenters;

import com.car.carcommunity.Models.Locations.Location;

import java.util.ArrayList;

/**
 * Created by zezo_ on 15/08/2017.
 */

public interface SearchLocation {

    ArrayList<Location> GetServiceLocations();

}
