package com.car.carcommunity.Views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.car.carcommunity.R;

public class WelcomeActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_welcome);
  }

  public void openSignupForm(View view) {
    Intent signup = new Intent(this, SignupActivity.class);
    startActivity(signup);
  }

  public void loginWithFacebook(View view) {
  }

  public void loginWithGmail(View view) {
  }

  public void openLoginForm(View view) {
    Intent login = new Intent(this, LoginActivity.class);
    startActivity(login);
  }

}
