package com.car.carcommunity.Models.Locations;

/**
 * Created by zezo_ on 15/08/2017.
 */

public class GasStationLocation extends Location {

    public GasStationLocation(Long latitude, Long longitude, Long address) {
        super(latitude, longitude, address);
    }

    public GasStationLocation(Long latitude, Long longitude, Long address, String phone) {
        super(latitude, longitude, address, phone);
    }
}
