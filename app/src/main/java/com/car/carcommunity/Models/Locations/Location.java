package com.car.carcommunity.Models.Locations;

/**
 * Created by zezo_ on 15/08/2017.
 */

public abstract class Location {

    private Long latitude;
    private Long longitude;
    private Long address;
    private String phone;


    public Location(Long latitude, Long longitude, Long address) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
    }

    public Location(Long latitude, Long longitude, Long address, String phone) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
        this.phone = phone;
    }

    public Long getLatitude() {
        return latitude;
    }

    public void setLatitude(Long latitude) {
        this.latitude = latitude;
    }

    public Long getLongitude() {
        return longitude;
    }

    public void setLongitude(Long longitude) {
        this.longitude = longitude;
    }

    public Long getAddress() {
        return address;
    }

    public void setAddress(Long address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
