package com.car.carcommunity.Models.Locations;

/**
 * Created by zezo_ on 15/08/2017.
 */

public class CarMaintenanceLocation extends Location {

    private int rate;
    private String maintenanceType; // Mechanic Electrician CarBody ...

    public CarMaintenanceLocation(Long latitude, Long longitude, Long address, String phone, String maintenanceType) {
        super(latitude, longitude, address, phone);
        this.maintenanceType = maintenanceType;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getMaintenanceType() {
        return maintenanceType;
    }

    public void setMaintenanceType(String maintenanceType) {
        this.maintenanceType = maintenanceType;
    }
}
