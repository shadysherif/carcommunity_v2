package com.car.carcommunity.Models.Locations;

/**
 * Created by zezo_ on 15/08/2017.
 */

public class CarAccessoriesLocation extends Location {


    public CarAccessoriesLocation(Long latitude, Long longitude, Long address, String phone) {
        super(latitude, longitude, address, phone);
    }
}
