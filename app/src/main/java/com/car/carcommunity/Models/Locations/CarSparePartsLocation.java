package com.car.carcommunity.Models.Locations;

/**
 * Created by zezo_ on 15/08/2017.
 */

public class CarSparePartsLocation extends Location {

    private String brandName;

    public CarSparePartsLocation(Long latitude, Long longitude, Long address, String phone) {
        super(latitude, longitude, address, phone);
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
